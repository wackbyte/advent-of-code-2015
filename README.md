# Advent of Code 2015

My solutions for Advent of Code 2015.

You can run this project with the help of [`cargo-aoc`](https://github.com/gobanos/cargo-aoc).
